import { AppBar, Box, Button, Container, Toolbar } from "@mui/material";
import { authProvider } from "./authProvider";
import UserDetailsTable from "./components/UserDetails";



export default function MainApp() {

  const handleLogout = async () => {
    try {
      await authProvider.logout();
    } catch (error) {
      console.error(error);
    }
  }

  return <Box>
    <AppBar color="default">
      <Toolbar style={{ justifyContent: "flex-end" }}>
        <Button variant="contained" onClick={handleLogout}>Logout</Button>
      </Toolbar>
    </AppBar>
    <Container style={{ marginTop: "70px" }}>
      <UserDetailsTable />
    </Container>
  </Box>
}