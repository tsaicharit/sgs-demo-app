import './App.css';
import { authProvider } from './authProvider';
import { Fragment, useEffect, useState } from 'react';
import { Box, Button, Container } from '@mui/material';
import MainApp from './MainApp';

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoadingAuthentication, setIsLoadingAuthentication] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        const { idToken: { rawIdToken } } = await authProvider.getIdToken();
        if (rawIdToken) {
          setIsAuthenticated(true);
        }
        setIsLoadingAuthentication(false);
      } catch (error) {
        console.error(error);
        setIsLoadingAuthentication(false);
      }
    })()
  }, [])

  const handleForceLogin = async () => {
    try {
      await authProvider.loginPopup();
      setIsAuthenticated(true);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <Fragment>
      <Box >
        {
          isLoadingAuthentication ?
            <div style={{ display: "flex", alignItems: "center", width: "100%", justifyContent: "center", minHeight: "100vh" }}>Verifying Authentication...</div> : null
        }
        {
          !isLoadingAuthentication && !isAuthenticated ?
            <div style={{ display: "flex", alignItems: "center", width: "100%", justifyContent: "center", minHeight: "100vh" }}>
              <Button onClick={handleForceLogin}>Login to Microsoft</Button>
            </div> : null
        }
        {isAuthenticated && <MainApp />}
      </Box>
    </Fragment>
  );
}

export default App;
