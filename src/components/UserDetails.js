import { useEffect, useState } from "react"
import { authProvider } from "../authProvider"
import { Container, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";

export default function UserDetailsTable() {

  const detailsArr = [
    { label: "Name", prop: "name" },
    { label: "Username", prop: "userName" },
    { label: "Account Identifier", prop: "accountIdentifier" },
    { label: "Environment", prop: "environment" },
    { label: "Home Account Identifier", prop: "homeAccountIdentifier" }
  ]

  const [userData, setUserData] = useState({});

  useEffect(() => {
    (async () => {
      try {
        const data = authProvider.getAccount();
        setUserData(data);
      } catch (error) {
        console.error(error);
      }
    })()
  }, [])

  return <Container>
    <Typography variant="h5">User Data</Typography>
    <TableContainer style={{ marginTop: "1rem" }}>
      <TextField placeholder="Search" size="small" fullWidth />
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Label</TableCell>
            <TableCell>Value</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            detailsArr.map((item, index) => (
              <TableRow key={item.prop + index}>
                <TableCell>{item.label}</TableCell>
                <TableCell>{userData[item.prop]}</TableCell>
              </TableRow>
            ))
          }
        </TableBody>
      </Table>
    </TableContainer>
  </Container>
}